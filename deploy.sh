#! /bin/sh

set -e
git pull origin master
cd ./client
yarn build
docker-compose build
docker-compose up -d
